﻿using System;
using System.Collections.Generic;
using WcfClient.EchoServiceReference;

namespace WcfClient
{
    class Program
    {
        private static long Benchmark(string endpointConfigurationName)
        {
            IEchoService serviceClient = new EchoServiceClient(endpointConfigurationName);
            var watch = System.Diagnostics.Stopwatch.StartNew();
            for (var i = 0; i < 2000; i++)
            {
                serviceClient.GetData(i);
            }

            watch.Stop();
            return watch.ElapsedMilliseconds;
        }

        private static void Main(string[] args)
        {
            int? proxyPort = null; // 18000;
            if (!proxyPort.HasValue)
            {
                var configurationNames = new List<string>
                {
                    "BasicHttpBinding_IEchoService", "NetNamedPipeBinding_IEchoService",
                    "NetTcpBinding_IEchoService", "WSHttpBinding_IEchoService"
                };
                foreach (var configurationName in configurationNames)
                {
                    Console.WriteLine($"{configurationName}: {Benchmark(configurationName)}");
                }

                Console.WriteLine("\nBenchmarking has been completed");
            }
            else
            {
                // mitmweb --web-port 28000 --listen-port 18000 --mode reverse:http://localhost:13044
                new EchoServiceClient("BasicHttpBinding_IEchoService",
                    $"http://localhost:{proxyPort.Value}/our/service/basic").GetData(0);
                new EchoServiceClient("WSHttpBinding_IEchoService",
                    $"http://localhost:{proxyPort.Value}/our/service/ws").GetData(0);
                Console.WriteLine("Querying through proxy has been completed");
            }

            Console.ReadLine();
        }
    }
}