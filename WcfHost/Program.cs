﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace WcfHost
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceMetadataBehavior behavior = new ServiceMetadataBehavior
            {
                HttpGetEnabled = true,
                MetadataExporter = { PolicyVersion = PolicyVersion.Policy15 }
            };
            var host = new ServiceHost(typeof(WcfServiceLibrary.EchoService), new Uri("http://localhost:13044/our/service"));
            host.Description.Behaviors.Add(behavior);
            host.AddServiceEndpoint(typeof(WcfServiceLibrary.IEchoService), new BasicHttpBinding(), "basic");
            host.AddServiceEndpoint(typeof(WcfServiceLibrary.IEchoService), new WSHttpBinding(), "ws");
            host.AddServiceEndpoint(typeof(WcfServiceLibrary.IEchoService), new NetTcpBinding(), "net.tcp://localhost:13054/our/service/tcp");
            host.AddServiceEndpoint(typeof(WcfServiceLibrary.IEchoService), new NetNamedPipeBinding(), "net.pipe://localhost/our/service/pipe");
            host.Open();
            Console.WriteLine("Server is running");
            Console.ReadLine();
        }
    }
}
